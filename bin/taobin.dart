import 'dart:io';

import 'package:taobin/taobin.dart' as taobin;

class Taobin {
  List<String> menu = ["1.Koko", "2.Greentea", "3.Whey protein", "4.Water"];
  List<int> price = [50, 40, 60, 10];
  Taobin() {}

  ShowMenu() {
    print(menu);
    print("Select your drinks by enter the number.");
  }

  ShowWelcome() {
    print("Welcome! Let's enjoy with my menu.");
  }

  Buy(int number) {
    print("you select ${menu[number - 1].substring(2)}");
    print("${menu[number - 1].substring(2)} is ${price[number - 1]} baht");
    print("enter money");
    int money = int.parse(stdin.readLineSync()!);
    print("got $money baht");
    var success = false;
    while (success == false) {
      if (money > price[number - 1]) {
        int change = money - price[number - 1];
        print("change is $change");
        print("Thank you!");
        return;
      } else if (money == price[number - 1]) {
        print("Thank you!");
        return;
      } else {
        print("not enough money. please enter more");
        int more = int.parse(stdin.readLineSync()!);
        money = money + more;
        print("got $money baht");
      }
    }
  }
}

class Member {
  int score = 0;
  var number;
  List<String> numberPhone = [];
  List<int> point = [];

  Member() {
    this.number = number;
  }

  GetNumber() {
    return number;
  }

  UpdateScore(String phone) {
    if (numberPhone.isEmpty) {
      numberPhone.add(phone);
      point.add(1);
      print("your score is ${point.elementAt(0)}");
    } else {
      for (int i = 0; i < numberPhone.length; i++) {
        if (numberPhone[i] == phone) {
          point[i] = point[i] + 1;
          print("your score is ${point[i]}");
          break;
        } else {
          numberPhone.add(phone);
          point.add(1);
          print("your score is ${point.last}");
          break;
        }
      }
    }
  }
}

void main(List<String> arguments) {
  var end = false;
  Taobin taobin = new Taobin();
  Member m1 = Member();
  while (end == false) {
    taobin.ShowWelcome();
    taobin.ShowMenu();
    int number = int.parse(stdin.readLineSync()!);
    taobin.Buy(number);
    print("Enter your phone number for score");
    String phone = stdin.readLineSync()!;
    m1.UpdateScore(phone);
    print("please wait a moment");
    print("you need a drink more? (y/n)");
    String more = stdin.readLineSync()!;
    if (more == "n") {
      break;
    }
  }
}
